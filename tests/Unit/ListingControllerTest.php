<?php

namespace Tests\Unit;

use App\Http\Controllers\ListingController;
use App\Models\User;
use App\Models\Listing;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Tests\TestCase;

class ListingControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_user_can_create_listing()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->post('/listings', [
            'title' => 'Test Listing',
            'company' => 'Test Company',
            'location' => 'Test Location',
            'website' => 'http://example.com',
            'email' => 'info@example.com',
            'tags' => 'tag1,tag2',
            'description' => 'Example Description'
        ]);

        $response->assertRedirect('/');
        $this->assertDatabaseHas('listings', ['title' => 'Test Listing']);
    }

    public function test_unauthenticated_user_cannot_create_listing()
    {
        $response = $this->post('/listings', [
            'title' => 'Test Listing',
            'company' => 'Test Company',
            'location' => 'Test Location',
            'website' => 'http://example.com',
            'email' => 'info@example.com',
            'tags' => 'tag1,tag2',
            'description' => 'Example Description'
        ]);

        $response->assertRedirect('/login');
        $this->assertDatabaseMissing('listings', ['title' => 'Test Listing']);
    }

    public function test_listing_creation_with_invalid_data()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->post('/listings', []);

        $response->assertSessionHasErrors(['title', 'company', 'location', 'website', 'email', 'tags', 'description']);
    }
}
