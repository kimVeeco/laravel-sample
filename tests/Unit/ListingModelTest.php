<?php

namespace Tests\Unit;

use App\Models\Listing;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ListingModelTest extends TestCase
{
    use RefreshDatabase;

    public function test_listing_can_be_created()
    {
        $user = User::factory()->create();
        $listing = Listing::factory()->create(['user_id' => $user->id]);

        $this->assertDatabaseHas('listings', [
            'id' => $listing->id,
            'title' => $listing->title,
            // Add more assertions for other attributes
        ]);
    }

    public function test_listing_can_be_updated()
    {
        $user = User::factory()->create();
        $listing = Listing::factory()->create(['user_id' => $user->id]);

        $listing->update(['title' => 'Updated Title']);

        $this->assertDatabaseHas('listings', ['title' => 'Updated Title']);
    }

    public function test_listing_can_be_deleted()
    {
        $user = User::factory()->create();
        $listing = Listing::factory()->create(['user_id' => $user->id]);

        $listing->delete();

        $this->assertDatabaseMissing('listings', ['id' => $listing->id]);
    }
}
