<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_register()
    {
        $response = $this->post('/users', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $this->assertDatabaseHas('users', ['email' => 'john@example.com']);
    }

    public function test_user_registration_with_invalid_data()
    {
        $response = $this->post('/users', []);

        $response->assertSessionHasErrors(['name', 'email', 'password']);
    }

    public function test_user_registration_with_duplicate_email()
    {
        $user = User::factory()->create();

        $response = $this->post('/users', [
            'name' => 'John Doe',
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertSessionHasErrors(['email']);
    }
}
